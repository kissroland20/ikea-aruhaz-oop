Feladat:

Írj programot (objektum orientáltan megtervezve), ami képes nyilvántartani műszaki cikkeket az IKEA áruház számára. 
Minden műszaki cikknek pontosan egy

- típusa (olcsó, átlagos, drága, luxus, népszerű), 
- gyártója (String), 
- vonalkódja, 
- ára (int) van 
- és tudjuk a regisztrálás dátumát (ami a felvétel napja). 
 
A termékek a vonalkódok sorszáma alapján összehasonlíthatók.
A műszaki cikkeknek 3 speciális típusa van, minden termék ebben a 3 csoport valamelyikében foglal helyet: 
konyhai eszközök, szórakoztató eszközök és szépségápolással kapcsolatos gépek. 
A konyhai eszközöket fel lehet emelni és be lehet kapcsolni. 
A szórakoztató eszközöket is be lehet kapcsolni, viszont 5 bekapcsolás után valamilyen hibát jeleznek (saját kivételosztály szükséges). 
A harmadik csoportnak van súlya kg-ban.
A raktárba érkezhetnek termékek és el is lehet őket adni. 

Az alábbi parancsokat a konzolon lehet megadni:

ADD vonalkód típus gyártó ár speciális_csoport → egy termék felvétele
REMOVE vonalkód → termék törlése
REPORT → report generálása
REPORT esetén az alábbi adatok jelennek meg a konzolon:
a)    Hány termék lett kimentve?
b)    Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?
